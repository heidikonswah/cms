const { Keystone } = require('@keystonejs/keystone');
const { PasswordAuthStrategy } = require('@keystonejs/auth-password');
const {
	Text,
	Select,
	Password,
	Integer,
	Url,
	CalendarDay,
	Relationship,
	File,
	Slug,
	OEmbed
} = require('@keystonejs/fields');
const { LocalFileAdapter, CloudinaryAdapter } = require('@keystonejs/file-adapters');
const { Wysiwyg } = require('@keystonejs/fields-wysiwyg-tinymce');
const { GraphQLApp } = require('@keystonejs/app-graphql');
const { AdminUIApp } = require('@keystonejs/app-admin-ui');
const { MongooseAdapter: Adapter } = require('@keystonejs/adapter-mongoose');
const fetch = require('node-fetch');
const oEmbed = require('oembed-spec');
const { expressCspHeader, SELF, INLINE } = require('express-csp-header');

const MongoStore = require('connect-mongo')(require('express-session'));
const adapter = new Adapter({
	mongoUri: process.env.MONGO_PASS
		? `mongodb+srv://cms:${process.env.MONGO_PASS}@cms-xlnet.gcp.mongodb.net/cms?retryWrites=true&w=majority`
		: 'mongodb://localhost/cms'
});

// Access control functions
const access = {
	userIsAdmin: ({ authentication: { item: user } }) => (user ? user.adminLevel === 'Admin' : false),
	editor: ({ authentication: { item: user } }) => Boolean(user),
	allowEditVolunteer: auth => {
		if (access.userIsAdmin(auth)) return true;
		const user = auth.authentication.item;
		if (user) {
			if (user.adminLevel === 'Head') return { adminLevel: 'Volunteer' };
		}
		return false;
	}
};

const keystone = new Keystone({
	name: 'IEEE SSCS AlexSC',
	cookieSecret: process.env.COOKIE_SECRET,
	sessionStore: new MongoStore({
		mongooseConnection: adapter.mongoose.connection,
		ttl: 15 * 24 * 60 * 60 // 15 days
	}),
	adapter,
	queryLimits: {
		maxTotalResults: 200
	},
	defaultAccess: {
		read: true,
		update: access.editor,
		create: access.editor,
		delete: access.editor
	},
	onConnect:
		process.env.NODE_ENV === 'development'
			? keystone =>
					keystone
						.executeQuery(
							`
mutation { createUser ( data: { name: "Admin", username: "admin", adminLevel: Admin, password: "goodluck" } ) { id } }
`
						)
						.catch(console.error)
			: null
});

let fileAdapter, staticApp;
if (process.env.CLOUDINARY_API_KEY && process.env.CLOUDINARY_API_SECRET) {
	fileAdapter = new CloudinaryAdapter({
		cloudName: 'sscsalex',
		apiKey: process.env.CLOUDINARY_API_KEY,
		apiSecret: process.env.CLOUDINARY_API_SECRET
	});
	staticApp = [];
} else {
	let paths = {
		src: 'dev/static',
		path: '/static'
	};
	fileAdapter = new LocalFileAdapter(paths);
	const { StaticApp } = require('@keystonejs/app-static');
	staticApp = [new StaticApp(paths)];
}

const invokeHook = () =>
	fetch(process.env.FRONTEND_BUILD_HOOK || 'http://localhost:8000/__refresh', { method: 'POST' }).catch(
		console.error
	);

const defaultHooks = {
	afterChange: invokeHook,
	afterDelete: invokeHook
};
const updateHooks = {
	afterChange: ({ operation }) => (operation === 'update' ? invokeHook() : Promise.resolve())
};

keystone.createList('User', {
	fields: {
		name: { type: Text },
		username: {
			type: Text,
			isUnique: true
		},
		adminLevel: {
			type: Select,
			isRequired: true,
			options: ['Admin', 'Head', 'Volunteer'],
			defaultValue: 'Volunteer',
			access: {
				update: access.userIsAdmin
			}
		},
		password: {
			type: Password
		}
	},
	access: {
		read: access.editor,
		update: auth => {
			if (access.allowEditVolunteer(auth)) return true;
			const user = auth.authentication.item;
			return user ? { id: user.id } : false;
		},
		create: auth => {
			if (access.userIsAdmin(auth)) return true;
			const user = auth.authentication.item;
			if (user) {
				if (user.adminLevel === 'Head') return auth.originalInput.adminLevel === 'Volunteer';
			}
			return false;
		},
		delete: access.allowEditVolunteer,
		auth: true
	}
});

keystone.createList('Tag', {
	fields: {
		name: {
			type: Text,
			isRequired: true
		}
	},
	hooks: updateHooks
});

keystone.createList('Volunteer', {
	fields: {
		role: { type: Text, isRequired: true },
		year: { type: Integer, isRequired: true },
		volunteers: {
			type: Relationship,
			ref: 'Bio',
			many: true
		}
	},
	labelResolver: ({ role, year }) => `${role} | ${year}`,
	hooks: updateHooks
});

keystone.createList('Bio', {
	fields: {
		name: {
			type: Text,
			isRequired: true
		},

		image: {
			type: File,
			adapter: fileAdapter
		},
		title: { type: Text },
		description: { type: Wysiwyg },
		email: { type: Text },
		linkedin: { type: Url },
		facebook: { type: Url },
		github: { type: Url },
		gitlab: { type: Url },
		slug: { type: Slug }
	},
	hooks: defaultHooks
});

const CustomWysiswyg = {
	type: Wysiwyg,
	editorConfig: {
		browser_spellecheck: true,
		toolbar: 'formatselect | bullist numlist indent outdent | link unlink | image table emoticons hr | code',
		quickbars_selection_toolbar:
			'bold italic underline strikethrough | h2 h3 h4 | quicklink blockquote removeformat',
		block_formats: 'Paragraph=p; Heading 1=h2; Heading 2=h3; Preformatted=pre',
		valid_elements:
			'a[rel|rev|charset|hreflang|tabindex|accesskey|type|name|href|target|title],' +
			'strong/b,em/i,strike,u,-ol[type|compact],-ul[type|compact],-li,' +
			'img[longdesc|usemap|src|border|alt=|title|hspace|vspace|width|height|align],' +
			'-sub,-sup,-blockquote,-table[frame|rules|align|summary],tbody,thead,tfoot,' +
			'-tr[rowspan|align|valign],td[colspan|rowspan|align|valign|scope],' +
			'th[colspan|rowspan|align|valign|scope],' +
			'caption,-code,-pre,address,-h2,-h3,-h4,-h5,-h6,hr,dd,dl,dt,cite,abbr,' +
			'del[datetime|cite],ins[datetime|cite],object[data|name|type],param[name|value],' +
			'embed[height|width|src|type],bdo,col[align|char|charoff|span|valign],' +
			'colgroup[align|char|charoff|span|valign],kbd,q[cite],samp,small,var,big'
	}
};

const CourseFields = {
	name: {
		type: Text,
		isRequired: true
	},
	image: {
		type: File,
		adapter: fileAdapter
	},
	description: CustomWysiswyg,
	instructors: {
		type: Relationship,
		ref: 'Bio',
		many: true
	},
	start: { type: CalendarDay },
	tags: {
		type: Relationship,
		ref: 'Tag',
		many: true
	},
	registrationLink: { type: Url },
	slug: { type: Slug }
};

keystone.createList('Course', {
	fields: CourseFields,
	hooks: defaultHooks
});

keystone.createList('TwoHoursPerWeek', {
	fields: CourseFields,
	hooks: defaultHooks
});

keystone.createList('Article', {
	fields: {
		title: {
			type: Text,
			isRequired: true
		},
		image: {
			type: File,
			adapter: fileAdapter
		},
		authors: {
			type: Relationship,
			ref: 'Bio',
			many: true
		},
		content: CustomWysiswyg,
		posted: { type: CalendarDay },
		tags: {
			type: Relationship,
			ref: 'Tag',
			many: true
		},
		slug: { type: Slug }
	},
	labelField: 'title',
	hooks: defaultHooks
});

keystone.createList('Magazine', {
	fields: {
		title: {
			type: Text,
			isRequired: true
		},
		image: {
			type: File,
			adapter: fileAdapter
		},
		file: {
			type: File,
			adapter: fileAdapter
		},
		posted: { type: CalendarDay },
		tags: { type: Relationship, ref: 'Tag', many: true }
	},
	labelField: 'title',
	hooks: defaultHooks
});

keystone.createList('Gallery', {
	fields: {
		media: {
			type: OEmbed,
			adapter: {
				fetch: ({ url, ...parameters }) => oEmbed(url, parameters)
			},
			isRequired: true
		},
		tags: { type: Relationship, ref: 'Tag', many: true }
	},
	labelResolver: item => item.media.title
});

const authStrategy = keystone.createAuthStrategy({
	type: PasswordAuthStrategy,
	list: 'User',
	config: {
		identityField: 'username',
		secretField: 'password'
	}
});

module.exports = {
	keystone,
	apps: [new GraphQLApp(), new AdminUIApp({ enableDefaultRoute: true, authStrategy }), ...staticApp],
	configureExpress: app => {
		app.use((req, res, next) => {
			res.set({
				'X-Content-Type-Options': 'nosniff',
				'X-Frame-Options': 'DENY',
				'X-XSS-Protection': '1; mode=block'
			});
			next();
		});
		app.use(
			/\/((?!admin\/(graphiql|api)).)*/,
			expressCspHeader({
				directives: {
					'default-src': [SELF],
					'img-src': ['*'],
					'style-src': [SELF, INLINE],
					'frame-src': ['*']
				}
			})
		);
		app.set('trust proxy', true);
	},
	cors: {
		origin: process.env.NODE_ENV === 'development' ? true : 'https://admin.sscsalex.org',
		credentials: true
	}
};
